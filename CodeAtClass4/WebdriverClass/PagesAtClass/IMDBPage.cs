﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass.PagesAtClass
{
    class IMDBPage : BasePage
    {
        public IMDBPage(IWebDriver webDriver) : base(webDriver)
        {
        }

        public static IMDBPage Navigate(IWebDriver webDriver)
        {
            webDriver.Url = "https://www.imdb.com/";
            return new IMDBPage(webDriver);
        }

        public FindFilmWidget GetFindFilmWidget()
        {
            return new FindFilmWidget(Driver);
        }

        public ListOfFilmsWidget GetListOfFilmWidget()
        {
            return new ListOfFilmsWidget(Driver);
        }

        public FilmWidget GetFilmWidget()
        {
            return new FilmWidget(Driver);
        }

    }
}
