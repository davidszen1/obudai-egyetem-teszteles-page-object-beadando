﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    class FindFilmWidget : BasePage
    {
        public FindFilmWidget(IWebDriver driver) : base(driver)
        {
        }
        
        public IWebElement SuggestionSearch => Driver.FindElement(By.Id("suggestion-search")); 
        public IWebElement SuggestionSearchButton => Driver.FindElement(By.Id("suggestion-search-button"));

        public void SetRoute(string filmTitle)
        {
            SuggestionSearch.SendKeys(filmTitle);
            
        }

        public IMDBPage SearchFor(string filmTitle)
        {
            SetRoute(filmTitle);
            return ClickSuggestionSearcButton();
        }

        public IMDBPage ClickSuggestionSearcButton()
        {
            SuggestionSearchButton.Click();
            return new IMDBPage(Driver);
        }
    }
}
