﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    class ListOfFilmsWidget : BasePage
    {
        public ListOfFilmsWidget(IWebDriver driver) : base(driver)
        {
        }
        private IWebElement FilmTable => Driver.FindElement(By.CssSelector("table[class = 'findList']"));
        private List<IWebElement> Films => FilmTable.FindElements(By.XPath(".//tbody/tr/td/a")).ToList();

        public IMDBPage ClickFilmLink()
        {
             Films.First().Click();
            return new IMDBPage(Driver);
        }

      



    }
}
