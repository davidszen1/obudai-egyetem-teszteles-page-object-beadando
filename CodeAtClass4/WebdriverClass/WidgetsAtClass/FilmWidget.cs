﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;

namespace WebdriverClass.WidgetsAtClass
{
    class FilmWidget : BasePage
    {
        public FilmWidget(IWebDriver driver) : base(driver)
        {
        }
        private IWebElement QuickLinksBar => Driver.FindElement(By.Id("quicklinksBar"));

        public bool IsQuickLinksBarExists()
        {
            return QuickLinksBar.Displayed;
        }

    }


}
