﻿using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebdriverClass.PagesAtClass;
using WebdriverClass.WidgetsAtClass;

namespace WebdriverClass.Tests
{
    class BadandoPageObjectTest : TestBase
    {
        [Test, TestCaseSource("LocalizationData")]
        public void IMDBFindTest(string filmcim)
        {
            bool result = false;
            try
            {
                FindFilmWidget findFilmhWidget = IMDBPage.Navigate(Driver).GetFindFilmWidget();
                IMDBPage imdbPage = findFilmhWidget.SearchFor(filmcim);
                ListOfFilmsWidget listOfFilmsWidget = imdbPage.GetListOfFilmWidget();
                imdbPage = listOfFilmsWidget.ClickFilmLink();
                FilmWidget filmWidget = imdbPage.GetFilmWidget();
                result = filmWidget.IsQuickLinksBarExists();
                
            }
            catch (Exception)
            {
                ScreenShot(filmcim);
            }
            finally
            {
                Assert.IsTrue(result);
            }
            

        }
        private void ScreenShot(string filmcim)
        {
            string baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string screenshotName = baseDirectory + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + "_error.png";
            var screenshot = ((ITakesScreenshot)Driver).GetScreenshot();
            screenshot.SaveAsFile( screenshotName, ScreenshotImageFormat.Png);
        }

        static IEnumerable LocalizationData()
        {
            return Films;  
        }

        static List<string> Films = new List<string>() { "star wars", "lord of the rings", "harry potter", "shfsnjd" };

    }
}
